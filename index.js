const ChatApp = require("chatApp");
const listenChat = require("listenChat");
const ansverChat = require("ansverChat");
const closeChatInTime = require("closeChatInTime");
const unsubChat = require("unsubChat");


let chatOnMessage = (message) => {
  console.log(message);
};

let webinarChat =  new ChatApp('webinar');
let facebookChat = new ChatApp('facebook');
let vkChat = new ChatApp('vk');
vkChat.getMaxListeners(2);

listenChat(webinarChat, chatOnMessage);
// webinarChat.listenChat(chatOnMessage);
ansverChat(webinarChat);
// webinarChat.ansverChat();
listenChat(facebookChat, chatOnMessage);
// facebookChat.listenChat(chatOnMessage);
listenChat(vkChat, chatOnMessage);
// vkChat.listenChat(chatOnMessage);
ansverChat(vkChat);
// vkChat.ansverChat();

// webinarChat.on('message', chatOnMessage);
// facebookChat.on('message', chatOnMessage);
// vkChat.on('message', chatOnMessage);

vkChat.once('close', (message) => {
  vkChat.removeAllListeners('message');
  console.log(message);
});

// Закрыть вконтакте
/*setTimeout( ()=> {
  console.log('Закрываю вконтакте...');
vkChat.removeListener('message', chatOnMessage);
}, 10000 );*/


// Закрыть фейсбук
/*setTimeout( ()=> {
  console.log('Закрываю фейсбук, все внимание — вебинару!');
facebookChat.removeListener('message', chatOnMessage);
}, 15000 );*/

closeChatInTime(facebookChat, 15);

unsubChat(webinarChat, chatOnMessage, 30);


vkChat.close();
